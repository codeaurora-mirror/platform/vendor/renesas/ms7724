# config.mk
#
# Product-specific compile-time definitions.
#

DISABLE_DEXPREOPT := true

# The generic product target doesn't have any hardware-specific pieces.
TARGET_NO_BOOTLOADER := true
TARGET_NO_KERNEL := true
TARGET_NO_RADIOIMAGE := true
TARGET_CPU_ABI := sh4abi
TARGET_PROVIDES_INIT_RC := true
TARGET_BOOTLOADER_BOARD_NAME := ms7724
USE_CAMERA_STUB := false
HAVE_HTC_AUDIO_DRIVER := false
BOARD_USES_GENERIC_AUDIO := false
BOARD_USES_ALSA_AUDIO := true
BUILD_WITH_ALSA_UTILS := true
